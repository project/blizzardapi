<?php

/**
 * @file
 * Allows users to log in using the Battle.net service.
 */

/**
 * The path used for authentication.
 */
define('BLIZZARDAPI_LOGIN_PATH', 'blizzardapi/login');
/**
 * The User-Agent used when making authentication requests. This automated
 * agent format allows Blizzard to report an issue if something goes wrong.
 */
define('BLIZZARDAPI_LOGIN_UA', 'Drupal (+https://www.drupal.org/project/blizzardapi)');

/**
 * Vistors may register normally or by using a Battle.net account.
 */
define('BLIZZARDAPI_LOGIN_REGISTER_NORMAL', 0);
/**
 * Visitors must register using a Battle.net account.
 */
define('BLIZZARDAPI_LOGIN_REGISTER_BNET_ONLY', 1);

/**
 * A list of ciphers used by cURL (at this time anyways).
 *
 * Users should consider the lists provided by Mozilla, which emphasize perfect
 * forward secrecy.
 */
define('BLIZZARDAPI_LOGIN_CIPHERS', 'ALL:!EXPORT:!EXPORT40:!EXPORT56:!aNULL:!LOW:!RC4');
/**
 * A list of ciphers for services that need more backward compatibility.
 *
 * This is the recommended configuration.
 */
define('BLIZZARDAPI_LOGIN_CIPHERS_MOZ_INTERMEDIATE', 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:' .
  'ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:' .
  'kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:' .
  'ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:' .
  'DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:' .
  'AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:' .
  '!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA');
/**
 * A list of ciphers for services that need a very high level of security.
 *
 * These ciphers may not be supported on all sites.
 */
define('BLIZZARDAPI_LOGIN_CIPHERS_MOZ_MODERN', 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:' .
  'ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:' .
  'kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:' .
  'ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:' .
  'DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:' .
  '!aNULL:!eNULL:!EXPORT:!DES:!RC4:!3DES:!MD5:!PSK');

/**
 * Implements hook_libraries_info().
 */
function blizzardapi_login_libraries_info() {
  $libraries['battlenet-google-client'] = array(
    'name' => t('Battle.net for Google API Clients'),
    'vendor url' => 'https://github.com/mattacosta/battlenet-google-client',
    'download url' => 'https://github.com/mattacosta/battlenet-google-client/releases',
    'version arguments' => array(
      'file' => 'BattleNetConfig.php',
      'pattern' => '/LIBVER = \'([\d.]+(?:-beta)?)\'/',
      'lines' => 30
    ),
    'files' => array(
      'php' => array('BattleNetAccount.php', 'BattleNetAuth.php', 'BattleNetConfig.php')
    ),
    'dependencies' => array(
      'google-api-php-client'
    )
  );
  $libraries['google-api-php-client'] = array(
    'name' => t('Google APIs Client Library for PHP'),
    'vendor url' => 'https://developers.google.com/api-client-library/php/',
    'download url' => 'https://github.com/google/google-api-php-client/releases',
    'path' => 'src',
    'version arguments' => array(
      'file' => 'src/Google/Client.php',
      'pattern' => '/LIBVER = "([\d.]+(?:-beta)?)"/',
      'lines' => 40
    ),
    'files' => array(
      'php' => array('Google/Client.php')
    ),
    'callbacks' => array(
      'pre-load' => array('blizzardapi_login_libraries_preload')
    )
  );
  return $libraries;
}

/**
 * Adds the google-api-php-client library path to PHP's include path.
 */
function blizzardapi_login_libraries_preload($library, $version = NULL, $variant = NULL) {
  $version = explode('.', $library['version'], 3);
  
  // Prior to version 1.1.0 there was no autoloading.
  if ($version[0] == 1 && $version[1] < 1) {
    set_include_path($library['library path'] . '/src/' . PATH_SEPARATOR . get_include_path());
  }
}

/**
 * Implements hook_cron().
 */
function blizzardapi_login_cron() {
  // Remove expired tokens. If done using hook_flush_caches(), all data would
  // be deleted during a module install/uninstall or from the performance page.
  // This may affect third-party modules that intend to take advantage of the
  // 30-day token lifetime to obtain API data.
  cache_clear_all(NULL, 'cache_blizzardapi_login');
}

/**
 * Implements hook_menu().
 */
function blizzardapi_login_menu() {
  $items[BLIZZARDAPI_LOGIN_PATH] = array(
    'title' => 'Battle.net Login',
    'page callback' => 'blizzardapi_login_page',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'includes/blizzardapi_login.pages.inc'
  );
  
  // Arguments: uid/timestamp/hash
  $items['blizzardapi/verify/%/%/%'] = array(
    'title' => 'Verify e-mail',
    'page callback' => 'blizzardapi_login_verify',
    'page arguments' => array(2, 3, 4),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'includes/blizzardapi_login.pages.inc',
  );
  
  $items['admin/config/services/blizzardapi/oauth'] = array(
    'title' => 'OAuth 2.0',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('blizzardapi_login_oauth'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_LOCAL_TASK
  );
  
  return $items;
}

/**
 * Implements hook_menu_site_status_alter().
 */
function blizzardapi_login_menu_site_status_alter(&$menu_site_status, $path) {
  // Allow access to the login page even if site is in offline mode.
  if ($menu_site_status == MENU_SITE_OFFLINE && user_is_anonymous() && $path == BLIZZARDAPI_LOGIN_PATH) {
    $menu_site_status = MENU_SITE_ONLINE;
  }
}

/**
 * Implements hook_block_info().
 */
function blizzardapi_login_block_info() {
  $block['login'] = array(
    'info' => t('Battle.net login'),
    'cache' => DRUPAL_NO_CACHE
  );
  return $block;
}

/**
 * Implements hook_block_view().
 */
function blizzardapi_login_block_view($delta = '') {
  global $user;
  $block = array();
  switch ($delta) {
    case 'login':
      if (!$user->uid && !(arg(0) == 'user' && !is_numeric(arg(1)))) {
        $block['subject'] = t('Battle.net login');
        $block['content'] = drupal_get_form('blizzardapi_login_user_login_block');
      }
      return $block;
  }
  return $block;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function blizzardapi_login_form_blizzardapi_settings_alter(&$form, &$form_state) {
  $form['login'] = array(
    '#type' => 'fieldset',
    '#title' => t('Login and registration')
  );
  
  // NOTE: Drupal does not always enfore the 'https' option.
  $redirect_url = url(BLIZZARDAPI_LOGIN_PATH, array('absolute' => TRUE, 'https' => TRUE));
  $form['login']['redirect_url'] = array(
    '#type' => 'item',
    '#title' => t('Redirect location'),
    '#markup' => str_replace('http://', 'https://', $redirect_url),
    '#description' => t('Register your application using this callback URL.')
  );
  
  $form['login']['blizzardapi_login_register'] = array(
    '#type' => 'checkbox',
    '#title' => t('Require Battle.net registration'),
    '#default_value' => variable_get('blizzardapi_login_register', BLIZZARDAPI_LOGIN_REGISTER_NORMAL),
    '#description' => t('New users must register using a Battle.net account. Existing users are not affected.'),
    '#access' => user_access('administer users')
  );
  
  $form['email'] = array(
    '#type' => 'item',
    '#title' => t('E-mails'),
    '#description' => t('Edit the e-mail messages sent to Battle.net users. ' .
      'Some settings apply to all users, and must be changed from the <a href="@account-settings">account settings</a> page. ' .
      'Available variables are: [site:name], [site:url], [user:name], [user:mail], [site:login-url], [site:url-brief], [user:edit-url], [user:one-time-login-url], [user:cancel-url].',
      array('@account-settings' => url('admin/config/people/accounts'))),
    '#access' => user_access('administer users')
  );
  
  $form['email_tabs'] = array(
    '#type' => 'vertical_tabs',
    '#access' => user_access('administer users')
  );
  
  $form['email_pending_approval'] = array(
    '#type' => 'fieldset',
    '#title' => t('Welcome (awaiting approval)'),
    '#collapsible' => TRUE,
    '#collapsed' => (variable_get('user_register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL) != USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL),
    '#description' => t('Edit the welcome e-mail messages sent to new members upon registering, when administrative approval is required.'),
    '#group' => 'email_tabs'
  );
  $form['email_pending_approval']['blizzardapi_login_register_pending_approval_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => blizzardapi_login_mail_text('register_pending_approval_subject', NULL, array(), FALSE),
    '#maxlength' => 180
  );
  $form['email_pending_approval']['blizzardapi_login_register_pending_approval_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#default_value' => blizzardapi_login_mail_text('register_pending_approval_body', NULL, array(), FALSE),
    '#rows' => 8
  );

  $form['email_no_approval_required'] = array(
    '#type' => 'fieldset',
    '#title' => t('Welcome (no approval required)'),
    '#collapsible' => TRUE,
    '#collapsed' => (variable_get('user_register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL) != USER_REGISTER_VISITORS),
    '#description' => t('Edit the welcome e-mail messages sent to new members upon registering, when no administrator approval is required.'),
    '#group' => 'email_tabs'
  );
  $form['email_no_approval_required']['blizzardapi_login_register_no_approval_required_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => blizzardapi_login_mail_text('register_no_approval_required_subject', NULL, array(), FALSE),
    '#maxlength' => 180
  );
  $form['email_no_approval_required']['blizzardapi_login_register_no_approval_required_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#default_value' => blizzardapi_login_mail_text('register_no_approval_required_body', NULL, array(), FALSE),
    '#rows' => 15
  );
  
  $form['email_activated'] = array(
    '#type' => 'fieldset',
    '#title' => t('Account activation'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('Enable and edit e-mail messages sent to users upon account activation (when an administrator activates an account of a user who has already registered, on a site where administrative approval is required).'),
    '#group' => 'email_tabs'
  );
  $form['email_activated']['user_mail_status_activated_notify'] = array(
    '#type' => 'checkbox',
    '#title' => t('Notify user when account is activated.'),
    '#default_value' => variable_get('user_mail_status_activated_notify', TRUE),
    '#disabled' => TRUE
  );
  $form['email_activated']['settings'] = array(
    '#type' => 'container',
    '#states' => array(
      // Hide the additional settings when this email is disabled.
      'invisible' => array('input[name="user_mail_status_activated_notify"]' => array('checked' => FALSE))
    )
  );
  $form['email_activated']['settings']['blizzardapi_login_status_activated_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => blizzardapi_login_mail_text('status_activated_subject', NULL, array(), FALSE),
    '#maxlength' => 180
  );
  $form['email_activated']['settings']['blizzardapi_login_status_activated_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#default_value' => blizzardapi_login_mail_text('status_activated_body', NULL, array(), FALSE),
    '#rows' => 15,
  );
  
  $form['email_password_reset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Password recovery'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('Edit the e-mail messages sent to users who request a new password.'),
    '#group' => 'email_tabs',
    '#weight' => 10
  );
  $form['email_password_reset']['blizzardapi_login_password_reset_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => blizzardapi_login_mail_text('password_reset_subject', NULL, array(), FALSE),
    '#maxlength' => 180
  );
  $form['email_password_reset']['blizzardapi_login_password_reset_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#default_value' => blizzardapi_login_mail_text('password_reset_body', NULL, array(), FALSE),
    '#rows' => 12
  );
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function blizzardapi_login_form_user_login_alter(&$form, &$form_state) {
  $form['blizzardapi_login'] = array(
    '#type' => 'fieldset',
    '#title' => t('Battle.net login'),
    '#weight' => 101
  );
  $form['blizzardapi_login']['battlenet_region'] = array(
    '#type' => 'select',
    '#title' => t('Battle.net region'),
    '#options' => array(
      BlizzardApiLoginClient::REGION_US => t('Americas & Oceanic'),
      BlizzardApiLoginClient::REGION_EU => t('Europe'),
      BlizzardApiLoginClient::REGION_KR => t('Korea'),
      BlizzardApiLoginClient::REGION_TW => t('Taiwan'),
      BlizzardApiLoginClient::REGION_CN => t('China')
    ),
    '#default_value' => BlizzardApiLoginClient::REGION_US,
    '#required' => TRUE
  );
  $form['blizzardapi_login']['battlenet_login'] = array(
    '#type' => 'submit',
    '#value' => t('Log in with Battle.net'),
    '#limit_validation_errors' => array(array('battlenet_region')),
    '#validate' => array('blizzardapi_login_user_login_validate'),
    '#submit' => array('blizzardapi_login_user_login_submit')
  );
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function blizzardapi_login_form_user_profile_form_alter(&$form, &$form_state) {
  global $user;
  
  if ($form['#user_category'] != 'account') {
    return;
  }
  
  $form['blizzardapi_login'] = array(
    '#type' => 'fieldset',
    '#title' => t('Battle.net login'),
    '#collapsible' => TRUE
  );
  
  $account = $form_state['user'];
  if (!blizzardapi_login_is_battlenet_account($account)) {
    if ($user->uid != $account->uid) {
      $form['blizzardapi_login']['#description'] = t('Only the account owner may connect a Battle.net account.');
      return;
    }
    
    $form['blizzardapi_login']['battlenet_region'] = array(
      '#type' => 'select',
      '#title' => t('Battle.net region'),
      '#options' => array(
        BlizzardApiLoginClient::REGION_US => t('Americas & Southeast Asia'),
        BlizzardApiLoginClient::REGION_EU => t('Europe'),
        BlizzardApiLoginClient::REGION_KR => t('Korea'),
        BlizzardApiLoginClient::REGION_TW => t('Taiwan'),
        BlizzardApiLoginClient::REGION_CN => t('China')
      ),
      '#default_value' => BlizzardApiLoginClient::REGION_US,
      '#required' => TRUE
    );
    $form['blizzardapi_login']['battlenet_login'] = array(
      '#type' => 'submit',
      '#value' => t('Add a Battle.net account'),
      '#limit_validation_errors' => array(array('battlenet_region')),
      '#validate' => array('blizzardapi_login_user_login_validate'),
      '#submit' => array('blizzardapi_login_user_login_submit')
    );
  }
  else {
    // If a one-time login link has been used (or if you are an administrator
    // editing an account), then a current password is not required.
    if (isset($form['account']['current_pass'])) {
      $form['account']['current_pass']['#description'] .= '<br/>';
      $form['account']['current_pass']['#description'] .= t('NOTE: If you registered using a Battle.net account, then you do not have a password associated with this site.');
    }
    
    if (variable_get('blizzardapi_login_register', BLIZZARDAPI_LOGIN_REGISTER_NORMAL)) {
      unset($form['blizzardapi_login']);
      return;
    }
    
    $form['blizzardapi_login']['#description'] = t('WARNING: If you do not have a password associated with this site, then you will have to request a new password after disconnecting your Battle.net account.');
    $form['blizzardapi_login']['battlenet_disconnect'] = array(
      '#type' => 'submit',
      '#value' => t('Remove Battle.net account'),
      '#submit' => array('blizzardapi_login_form_user_profile_disconnect')
    );
  }
}

/**
 * Submit handler; Disconnects an attached Battle.net account.
 */
function blizzardapi_login_form_user_profile_disconnect($form, &$form_state) {
  // Remove the authmap entry.
  user_set_authmaps($form_state['user'], array('authname_blizzardapi_login' => FALSE));
  
  watchdog('blizzardapi_login', 'Disconnected a Battle.net account for %user.', array('%user' => format_username($form_state['user'])));
  drupal_set_message('Battle.net account disconnected.');
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function blizzardapi_login_form_user_register_form_alter(&$form, &$form_state) {
  $bnet_only = variable_get('blizzardapi_login_register', BLIZZARDAPI_LOGIN_REGISTER_NORMAL);
  if (!isset($_SESSION['blizzardapi_login']['battlenet_id']) && $bnet_only) {
    drupal_set_message(t('Only Battle.net accounts may be created on this site.'), 'error');
    drupal_goto();
  }
  
  if (isset($_SESSION['blizzardapi_login']['battlenet_id'])) {
    $form['battlenet_message'] = array(
      '#markup' => '<p>' . t('Finish creating your account by filling out the form below.') . '</p>',
      '#weight' => -11
    );
    
    // @todo May be able to pre-fill the username with a BattleTag.
    
    // Provide a random default password for Drupal.
    if (!variable_get('user_email_verification', TRUE)) {
      $form['account']['pass']['#type'] = 'hidden';
      $form['account']['pass']['#value'] = user_password();
    }
    
    // Remove the part about receiving a new password.
    $form['account']['mail']['#description'] = t('A valid e-mail address. All e-mails from the system will be sent to this address. ' .
      'The e-mail address is not made public and will only be used if you wish to receive certain news or notifications by e-mail.');
    
    $form['actions']['cancel'] = array(
      '#type' => 'submit',
      '#value' => t('Cancel'),
      '#limit_validation_errors' => array(),
      '#submit' => array('blizzardapi_login_form_user_register_cancel')
    );
  }
}

/**
 * Submit handler; Remove any saved Battle.net information after a partial
 * account registration attempt.
 */
function blizzardapi_login_form_user_register_cancel($form, &$form_state) {
  drupal_set_message(t('Account registration cancelled.'));
  unset($_SESSION['blizzardapi_login']);
  $form['redirect'] = '';
}

/**
 * Form builder; Generates an OAuth 2.0 settings form.
 */
function blizzardapi_login_oauth($form, &$form_state) {
  $client = variable_get('blizzardapi_login_client', 'BlizzardApiLoginDefaultClient');
  
  if (!isset($form_state['input']['op'])) {
    $insecure_php_version = version_compare(phpversion(), '5.6.0', '<');
    if ($client == 'BlizzardApiLoginDefaultClient') {
      $cafile = variable_get('blizzardapi_login_cafile', '');
      if ($insecure_php_version && empty($cafile)) {
        drupal_set_message(t('A CA file is required to use the currently selected authentication client.'), 'error');
      }
    }
    if ($client == 'BlizzardApiLoginGoogleClient') {
      $info = libraries_load('battlenet-google-client');
      if (!$info['loaded']) {
        drupal_set_message(t('Unable to load the selected authentication client, it may not be installed correctly.'), 'error');
      }
      if ($insecure_php_version && !function_exists('curl_init')) {
        drupal_set_message(t('Use of the cURL library is strongly recommended while using this authentication client with your PHP configuration.'), 'warning');
      }
    }
  }
  
  $form['oauth'] = array(
    '#type' => 'fieldset',
    '#title' => t('OAuth 2.0'),
    '#description' => t('This module supports changing the OAuth 2.0 client used for authentication to any compatible implementation. ' .
      'Before selecting a client, carefully read the provided documentation, as there may be security issues depending on your PHP installation.')
  );
  
  // @todo Dynamically create list of OAuth 2.0 clients?
  $form['oauth']['blizzardapi_login_client'] = array(
    '#type' => 'select',
    '#title' => t('Authentication client'),
    '#options' => array(
      'BlizzardApiLoginDefaultClient' => t('Blizzard API Default Client'),
      'BlizzardApiLoginGoogleClient' => t('Battle.net for Google API Clients')
    ),
    '#default_value' => $client
  );
  
  $form['oauth']['default_client'] = array(
    '#type' => 'fieldset',
    '#title' => t('Blizzard API Default Client settings'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE
  );
  $form['oauth']['default_client']['blizzardapi_login_cafile'] = array(
    '#type' => 'textfield',
    '#title' => t('CA file'),
    '#description' => t('The location of a file containing the public keys of trusted Certificate Authorities. ' .
      'This file is optional if you are using PHP 5.6 or later.<br/>' .
      'If needed, a bundle of root certificates from Mozilla is provided by the <a href="@curl-url">cURL authors</a>.',
      array('@curl-url' => 'http://curl.haxx.se/docs/caextract.html')),
    '#maxlength' => 255,
    '#default_value' => variable_get('blizzardapi_login_cafile', '')
  );
  $form['oauth']['default_client']['blizzardapi_login_ciphers'] = array(
    '#type' => 'textfield',
    '#title' => t('Ciphers'),
    '#description' => t('A list of ciphers which should or should not be used when establishing a secure connection. ' .
      'The format is described in <a href="@cipher-url">ciphers</a>.',
      array('@cipher-url' => 'https://www.openssl.org/docs/apps/ciphers.html#CIPHER-LIST-FORMAT')),
    '#maxlength' => 768,
    '#default_value' => variable_get('blizzardapi_login_ciphers', BLIZZARDAPI_LOGIN_CIPHERS_MOZ_INTERMEDIATE)
  );
  
  $form['oauth']['battlenet_google_client'] = array(
    '#type' => 'fieldset',
    '#title' => t('Battle.net for Google API Clients settings'),
    '#description' => t('There are no user-configurable settings.'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE
  );
  
  return system_settings_form($form);
}

/**
 * Validation handler; Checks for valid OAuth 2.0 configuration settings.
 */
function blizzardapi_login_oauth_validate($form, &$form_state) {
  $client = $form_state['values']['blizzardapi_login_client'];
  
  if ($client == 'BlizzardApiLoginDefaultClient') {
    $cafile = $form_state['values']['blizzardapi_login_cafile'];
    if (!empty($cafile) && !file_exists($cafile)) {
      form_set_error('blizzardapi_login_cafile', t('The specified CA file could not be found.'));
    }
    
    // Since drupal_http_request() eats connection warnings, it can be
    // extremely difficult to debug encryption errors from the specified
    // ciphers. One possibility is to create a test connection surrounded
    // by a custom error handler here and then warn the user.
  }
}

/**
 * Form builder; Generates a Battle.net login form.
 */
function blizzardapi_login_user_login_block() {
  $form['#attached']['js'][] = drupal_get_path('module', 'blizzardapi_login') . '/blizzardapi_login.js';
  $form['#attached']['library'][] = array('system', 'jquery.cookie');
  $form['#attributes']['class'][] = 'blizzardapi-login-from-cookie';
  $form['battlenet_region'] = array(
    '#type' => 'select',
    '#title' => t('Battle.net region'),
    '#options' => array(
      BlizzardApiLoginClient::REGION_US => t('Americas & Oceanic'),
      BlizzardApiLoginClient::REGION_EU => t('Europe'),
      BlizzardApiLoginClient::REGION_KR => t('Korea'),
      BlizzardApiLoginClient::REGION_TW => t('Taiwan'),
      BlizzardApiLoginClient::REGION_CN => t('China')
    ),
    '#default_value' => BlizzardApiLoginClient::REGION_US
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['battlenet_login'] = array(
    '#type' => 'submit',
    '#value' => t('Log in with Battle.net'),
    '#validate' => array('blizzardapi_login_user_login_validate'),
    '#submit' => array('blizzardapi_login_user_login_submit')
  );
  return $form;
}

/**
 * Validation handler; Set a login form error if an OAuth 2.0 client cannot be
 * created.
 */
function blizzardapi_login_user_login_validate($form, &$form_state) {
  $client = blizzardapi_login_client($form_state['values']['battlenet_region']);
  if (!isset($client)) {
    form_set_error('battlenet_login', t('Unable to load authentication library.'));
  }
}

/**
 * Submit handler; Initiate the OAuth 2.0 authentication flow.
 */
function blizzardapi_login_user_login_submit($form, &$form_state) {
  global $user;
  
  $destination = user_login_destination();
  $token = drupal_get_token('blizzardapi_login');
  
  if (!$user->uid) {
    user_cookie_save(array_intersect_key($form_state['values'], array_flip(array('battlenet_region'))));
  }
  
  $_SESSION['blizzardapi_login'] = array(
    'destination' => $destination['destination'],
    'region' => $form_state['values']['battlenet_region']
  );
  
  $scopes = blizzardapi_login_scopes();
  $client = blizzardapi_login_client($form_state['values']['battlenet_region'], $scopes);
  
  // Remove any destination overrides prior to the authorization redirect.
  if (isset($_GET['destination'])) {
    unset($_GET['destination']);
  }
  
  drupal_goto($client->getAuthorizeUrl($token));
}

/**
 * Implements hook_mail_alter().
 *
 * Replaces the text of certain e-mails for users with a Battle.net account.
 */
function blizzardapi_login_mail_alter(&$message) {
  // Just using $message['module'] == 'user' is another option here.
  switch ($message['id']) {
    case 'user_password_reset':
    case 'user_register_no_approval_required':
    case 'user_register_pending_approval':
    case 'user_status_activated':
      $account = $message['params']['account'];
      if (blizzardapi_login_is_battlenet_account($account)) {
        $subject = blizzardapi_login_mail_text($message['key'] . '_subject', $message['language'], array('user' => $account));
        $message['subject'] = $subject;
        $body = blizzardapi_login_mail_text($message['key'] . '_body', $message['language'], array('user' => $account));
        $message['body'][0] = $body;
      }
      break;
  }
}

/**
 * Provides the text used in mail sent or modified by this module.
 *
 * @param string $key
 *   A key specifying the mail text to return.
 * @param object $language
 *   (optional) The recipient's preferred language.
 * @param array $variables
 *   (optional) Token replacement data.
 * @param bool $replace
 *   (optional) If FALSE, text will be returned without token replacement.
 *
 * @return string
 *   The default mail text, or if a variable override has been set, that text
 *   instead.
 */
function blizzardapi_login_mail_text($key, $language = NULL, $variables = array(), $replace = TRUE) {
  $langcode = isset($language) ? $language->language : NULL;
  
  if ($custom_text = variable_get('blizzardapi_login_' . $key, FALSE)) {
    $text = $custom_text;
  }
  else {
    switch ($key) {
      case 'password_reset_subject':
        $text = t('Replacement login information for [user:name] at [site:name]', array(), array('langcode' => $langcode));
        break;
      case 'password_reset_body':
        $text = t("[user:name],

A request to reset the password for your account has been made at [site:name]. If this was not you, please disregard this e-mail.

Since you have registered on [site:name] using a third party, you may not have a password associated with us. Just log in using your Battle.net account:

[site:login-url]

If you do have a password associated with us, you may also log in by clicking this link or copying and pasting it to your browser:

[user:one-time-login-url]

This link can only be used once to log in and will lead you to a page where you can set your password. It expires after one day and nothing will happen if it's not used.

-- [site:name] team", array(), array('langcode' => $langcode));
        break;
      case 'register_no_approval_required_subject':
        $text = t('Account details for [user:name] at [site:name]', array(), array('langcode' => $langcode));
        break;
      case 'register_no_approval_required_body':
        $text = t("[user:name],

Thank you for registering at [site:name]. You may now verify your e-mail address by clicking this link or by copying and pasting it to your browser.

[blizzardapi:one-time-login-url]

-- [site:name] team", array(), array('langcode' => $langcode));
        break;
      case 'register_pending_approval_subject':
        $text = t('Account details for [user:name] at [site:name] (pending admin approval)', array(), array('langcode' => $langcode));
        break;
      case 'register_pending_approval_body':
        $text = t("[user:name],

Thank you for registering at [site:name]. Your application for an account is currently pending approval. Once it has been approved, you will receive another e-mail containing more information.

-- [site:name] team", array(), array('langcode' => $langcode));
        break;
      case 'status_activated_subject':
        $text = t('Account details for [user:name] at [site:name] (approved)', array(), array('langcode' => $langcode));
        break;
      case 'status_activated_body':
        // NOTE: The full URL token is required since using the [site:url-brief]
        // token would also remove 'https' from the link.
        $text = t("[user:name],

Your account at [site:name] has been activated. You may now log in at [site:url] using your Battle.net account.

-- [site:name] team", array(), array('langcode' => $langcode));
        break;
    }
  }
  
  if ($replace) {
    $token_options = array(
      'language' => $language,
      'callback' => 'blizzardapi_login_mail_tokens',
      'sanitize' => FALSE,
      'clear' => TRUE
    );
    return token_replace($text, $variables, $token_options);
  }
  
  return $text;
}

/**
 * Token callback; Replaces unsafe tokens in user mails.
 */
function blizzardapi_login_mail_tokens(&$replacements, $data, $options) {
  user_mail_tokens($replacements, $data, $options);
  if (isset($data['user'])) {
    $replacements['[blizzardapi:one-time-login-url]'] = blizzardapi_login_reset_url($data['user']);
  }
}

/**
 * Implements hook_user_insert().
 */
function blizzardapi_login_user_insert(&$edit, $account, $category) {
  // NOTE: Errors may occur during a previous login attempt, so check for a
  // Battle.net identifier, which is only set if everything was successful.
  if (isset($_SESSION['blizzardapi_login']['battlenet_id'])) {
    blizzardapi_login_set_authmap($account);
    unset($_SESSION['blizzardapi_login']);
  }
}

/**
 * Implements hook_user_login().
 */
function blizzardapi_login_user_login(&$edit, $account) {
  if (isset($_SESSION['blizzardapi_login'])) {
    unset($_SESSION['blizzardapi_login']);
  }
}

/**
 * ----------------------------------------------------------------------------
 * Utility functions.
 * ----------------------------------------------------------------------------
 */

/**
 * Generate the authmap for a Battle.net account.
 *
 * @param string $region
 *   A supported Battle.net login region.
 * @param int $id
 *   The account ID number.
 *
 * @return string
 *   An authmap for the account, or FALSE if an invalid region is specified.
 */
function _blizzardapi_login_authmap($region, $id) {
  $partition = '';
  // A simple if-else statement would not work here because if a new partition
  // is ever created, it would always fall into the 'else' and potentially
  // collide with another user in that partition.
  switch ($region) {
    case BlizzardApiLoginClient::REGION_US:
    case BlizzardApiLoginClient::REGION_EU:
    case BlizzardApiLoginClient::REGION_KR:
    case BlizzardApiLoginClient::REGION_TW:
      $partition = 'battle.net/';
      break;
    case BlizzardApiLoginClient::REGION_CN:
      $partition = 'battlenet.com.cn/';
      break;
  }
  if (empty($partition) || !is_numeric($id)) {
    return FALSE;
  }
  return $partition . $id;
}

/**
 * Create a new instance of the site's chosen OAuth 2.0 client implementation.
 *
 * @param string $region
 *   A supported Battle.net login region.
 * @param array $scopes
 *   (optional) A list of scopes to request during the login attempt.
 *
 * @return object
 *   A new BlizzardApiLoginClient instance or NULL if an error occured.
 */
function blizzardapi_login_client($region, $scopes = array()) {
  $class = variable_get('blizzardapi_login_client', 'BlizzardApiLoginDefaultClient');
  $client = NULL;
  
  try {
    $client = new $class($region, $scopes);
    if (!($client instanceof BlizzardApiLoginClient)) {
      $client = NULL;
    }
  }
  catch (Exception $e) {
    // Log the exception to make debugging easier.
    watchdog_exception('blizzardapi_login', $e);
  }
  
  return $client;
}

/**
 * Returns TRUE if the specified user is associated with a Battle.net account.
 */
function blizzardapi_login_is_battlenet_account($account) {
  if (isset($account->uid)) {
    $fields = array(':uid' => $account->uid, ':module' => 'blizzardapi_login');
    $aid = db_query('SELECT aid FROM {authmap} WHERE uid = :uid AND module = :module', $fields)->fetchField();
    if ($aid) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Creates a unique hash value for use in time-dependent per-user URLs.
 *
 * These hashes are slightly modified so as to be unusable on Drupal pages that
 * expect standard hashes, such as the password reset page.
 *
 * @param string $password
 *   The hashed user account password.
 * @param int $timestamp
 *   A UNIX timestamp, typically REQUEST_TIME.
 * @param int $login
 *   The UNIX timestamp of the user's last login.
 * @param int $uid
 *   The user ID of the user account.
 *
 * @return string
 *   A string that is safe for use in URLs and SQL statements.
 */
function blizzardapi_login_rehash($password, $timestamp, $login, $uid) {
  // Note: While the documentation says that the some parameters should be
  // integers, all parameter data is converted to a single data string for
  // use by drupal_hmac_base64().
  return user_pass_rehash($password, 'BNET' . $timestamp, $login, $uid);
}

/**
 * Generates a unique URL for a user to verify their e-mail address (and login).
 *
 * The URL parameters cannot be used to reset a user password.
 *
 * @param object $account
 *   A user account, which must contain the following properties:
 *   - uid: The user's ID number.
 *   - pass: The user's hashed password.
 *   - login: The UNIX timestamp of the user's last login.
 */
function blizzardapi_login_reset_url($account) {
  $timestamp = REQUEST_TIME;
  return url(
    "blizzardapi/verify/$account->uid/$timestamp/" . blizzardapi_login_rehash($account->pass, $timestamp, $account->login, $account->uid),
    array('absolute' => TRUE)
  );
}

/**
 * Returns all the scopes requested by other modules.
 */
function blizzardapi_login_scopes() {
  $scopes = &drupal_static(__FUNCTION__);
  if (!isset($scopes)) {
    $scopes = array();
    foreach (module_implements('blizzardapi_login_scopes') as $module) {
      $result = module_invoke($module, 'blizzardapi_login_scopes');
      foreach ($result as $scope) {
        if (!in_array($scope, $scopes)) {
          $scopes[] = $scope;
        }
      }
    }
  }
  return $scopes;
}

/**
 * Map an external account to an existing Drupal user.
 *
 * @param object $account
 *   A Drupal user object.
 */
function blizzardapi_login_set_authmap($account) {
  $region = $_SESSION['blizzardapi_login']['region'];
  $bnet_id = $_SESSION['blizzardapi_login']['battlenet_id'];
  
  // Blizzard's accounts are only unique within a partition.
  $authmap = _blizzardapi_login_authmap($region, $bnet_id);
  if ($authmap !== FALSE) {
    user_set_authmaps($account, array('authname_blizzardapi_login' => $authmap));
    watchdog('blizzardapi_login', 'Connected a Battle.net account for %name.', array('%name' => format_username($account)));
  }
  else {
    watchdog('blizzardapi_login', 'Session information did not contain valid parameters to connect a Battle.net account (region: %region).',
      array('%region' => $region), WATCHDOG_ERROR);
  }
}

/**
 * Gets a cached Battle.net OAuth 2.0 token for the specified user.
 *
 * @param object $user
 *   A Drupal user object.
 *
 * @return mixed
 *   An array containing an OAuth 2.0 access token or FALSE if there was no
 *   cached token.
 */
function blizzardapi_login_user_token($user) {
  if (!isset($user->uid)) {
    return FALSE;
  }
  
  $fields = array(':uid' => $user->uid, ':module' => 'blizzardapi_login');
  $authname = db_query('SELECT authname FROM {authmap} WHERE uid = :uid AND module = :module', $fields)->fetchField();
  if ($authname !== FALSE) {
    $cache = cache_get($authname, 'cache_blizzardapi_login');
    if ($cache !== FALSE) {
      if (REQUEST_TIME < $cache->expire || $cache->expire == CACHE_PERMANENT) {
        return $cache->data;
      }
    }
  }
  return FALSE;
}
